import logging
import os
import pytest
import time
import unittest

from selenium import webdriver
from selenium.webdriver.common.keys import Keys


class TestHackerNewsSearch:
    @pytest.fixture(scope='session')
    def browser(self):
        caps = {'browserName': os.getenv('BROWSER', 'chrome')}
        return webdriver.Remote(
            command_executor='http://selenium__standalone-chrome:4444/wd/hub',
            desired_capabilities=caps
        )

    @pytest.mark.test_1
    @pytest.mark.parametrize("iteration", range(5))
    def test_hackernews_search_for_testdrivenio(self, browser, iteration):
        logging.info("hey bro")
        browser.get('https://news.ycombinator.com')
        search_box = browser.find_element_by_name('q')
        search_box.send_keys('testdriven.io')
        search_box.send_keys(Keys.RETURN)
        time.sleep(3)  # simulate long running test

        assert 'testdriven.io' in browser.page_source

        posts = browser.find_elements_by_css_selector('.Story_title span')
        titles = [post.text for post in posts]

        logging.info(titles)

    @pytest.mark.test_2
    @pytest.mark.parametrize("iteration", range(5))
    def test_hackernews_search_for_selenium(self, browser, iteration):
        logging.info("lmao")
        browser.get('https://news.ycombinator.com')
        search_box = browser.find_element_by_name('q')
        search_box.send_keys('selenium')
        search_box.send_keys(Keys.RETURN)
        time.sleep(3)  # simulate long running test

        assert 'selenium' in browser.page_source

        posts = browser.find_elements_by_css_selector('.Story_title span')
        titles = [post.text for post in posts]

        logging.info(titles)
