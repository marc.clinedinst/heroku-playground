import json
import os
import requests

HEROKU_AUTH_TOKEN = os.environ.get("HEROKU_AUTH_TOKEN")

response = requests.post(
  "https://api.heroku.com/apps",
  headers={
    "Accept": "application/vnd.heroku+json; version=3",
    "Authorization": f"Bearer {HEROKU_AUTH_TOKEN}",
    "Content-Type": "application/json"
  }
)

print(response.json()["name"])
